import { Component, OnDestroy, OnInit } from '@angular/core';
import { CocktailModel } from '../shared/coctail.model';
import { CoctailService } from '../shared/coctail.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-coctail-list',
  templateUrl: './coctail-list.component.html',
  styleUrls: ['./coctail-list.component.css']
})
export class CoctailListComponent implements OnInit, OnDestroy{
  cocktails!: CocktailModel[];
  cocktailChangeSubscription!: Subscription;
  cocktailFetchingSubscription!: Subscription;
  isFetching: boolean = false;


  constructor(private cocktailsService: CoctailService) {
  }

  ngOnInit(): void {
    this.cocktails = this.cocktailsService.getCocktails();
    this.cocktailChangeSubscription = this.cocktailsService.cocktailChange.subscribe((cocktail: CocktailModel[]) => {
      this.cocktails = cocktail;
    });
    this.cocktailFetchingSubscription = this.cocktailsService.cocktailFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.cocktailsService.fetchCocktails();
  }


  ngOnDestroy() {
    this.cocktailChangeSubscription.unsubscribe();
    this.cocktailFetchingSubscription.unsubscribe();
  }

}
