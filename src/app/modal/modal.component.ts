import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CocktailModel } from '../shared/coctail.model';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  @Input() isOpen = true;
  @Output() close = new EventEmitter<void>();
  @Input() cocktail!: CocktailModel;

  onClose() {
    this.close.emit();
  }
}
