import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoctailListComponent } from './coctail-list/coctail-list.component';
import { NewCoctailComponent } from './new-coctail/new-coctail.component';

const routes: Routes = [
  {path: '', component: CoctailListComponent},
  {path: 'list', component: CoctailListComponent},
  {path: 'new', component: NewCoctailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
