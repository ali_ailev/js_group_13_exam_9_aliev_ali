import { Injectable } from '@angular/core';
import { map, Subject, tap } from 'rxjs';
import { CocktailModel } from './coctail.model';
import { HttpClient } from '@angular/common/http';


@Injectable()

export class CoctailService {
  cocktailChange = new Subject<CocktailModel[]>();
  cocktailFetching = new Subject<boolean>();
  cocktailLoading = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private cocktails: CocktailModel[] = [];

  fetchCocktails() {
    this.cocktailFetching.next(true);
    this.http.get<{ [id: string]: CocktailModel }>('https://cocktails-9-default-rtdb.firebaseio.com/Cocktails.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new CocktailModel(
            cocktailData.name,
            cocktailData.imageUrl,
            cocktailData.type,
            cocktailData.description,
            cocktailData.ingredients,
            cocktailData.cookDescription,
          );
        });
      }))
      .subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailChange.next(this.cocktails.slice());
        this.cocktailFetching.next(false);
      }, error => {
        this.cocktailFetching.next(false);
      });
  }

  getCocktails() {
    return this.cocktails.slice()
  }

  addCocktail(cocktail: CocktailModel) {
    const body = {
      name: cocktail.name,
      imageUrl: cocktail.imageUrl,
      type: cocktail.type,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      cookDescription: cocktail.cookDescription,
    };

    this.cocktailLoading.next(true);

    return this.http.post('https://cocktails-9-default-rtdb.firebaseio.com/Cocktails.json', body).pipe(
      tap(() => {
        this.cocktailLoading.next(false);
      }, error => {
        this.cocktailLoading.next(false);
      })
    );
  }
}


