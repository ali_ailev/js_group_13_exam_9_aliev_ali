import { IngredientModel } from './ingredient.model';

export class CocktailModel {
  constructor(
    public name: string,
    public imageUrl: string,
    public type: string,
    public description: string,
    public ingredients: IngredientModel[],
    public cookDescription: string,
  ) {
  }
}
