import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoctailListComponent } from './coctail-list/coctail-list.component';
import { NewCoctailComponent } from './new-coctail/new-coctail.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { CoctailBlockComponent } from './coctail-list/coctail-block/coctail-block.component';
import { CoctailService } from './shared/coctail.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';
import { ValidateUrlDirective } from './new-coctail/validate-url.directive';

@NgModule({
  declarations: [
    AppComponent,
    CoctailListComponent,
    NewCoctailComponent,
    ToolbarComponent,
    CoctailBlockComponent,
    ModalComponent,
    ValidateUrlDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CoctailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
