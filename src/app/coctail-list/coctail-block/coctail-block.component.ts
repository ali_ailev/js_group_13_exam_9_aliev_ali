import { Component, Input, OnInit } from '@angular/core';
import { CocktailModel } from '../../shared/coctail.model';

@Component({
  selector: 'app-coctail-block',
  templateUrl: './coctail-block.component.html',
  styleUrls: ['./coctail-block.component.css']
})
export class CoctailBlockComponent implements OnInit {
  @Input() cocktail!: CocktailModel;
  modalOpen = false;
  constructor() { }

  ngOnInit(): void {
  }


  openCheckoutModal() {
    this.modalOpen = true;
  }

  closeCheckoutModal() {
    this.modalOpen = false;
  }
}
