import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CocktailModel } from '../shared/coctail.model';
import { CoctailService } from '../shared/coctail.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-coctail',
  templateUrl: './new-coctail.component.html',
  styleUrls: ['./new-coctail.component.css']
})
export class NewCoctailComponent implements OnInit {
  cocktailForm!: FormGroup;
  isFetching = false;

  constructor(private cocktailService: CoctailService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imageUrl: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      description: new FormControl(''),
      ingredients: new FormArray([]),
      cookDescription: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    const name = this.cocktailForm.controls['name'].value;
    const imageUrl = this.cocktailForm.controls['imageUrl'].value;
    const type = this.cocktailForm.controls['type'].value;
    const description = this.cocktailForm.controls['description'].value;
    const ingredients = this.cocktailForm.controls['ingredients'].value;
    const cookDescription = this.cocktailForm.controls['cookDescription'].value;
    const cocktail = new CocktailModel(name, imageUrl, type, description, ingredients, cookDescription);
    this.cocktailService.addCocktail(cocktail).subscribe();

    void this.router.navigate(['/']);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return Boolean(
      field && field.touched && field.errors?.[errorType]
    );
  }

  fieldIngError(fieldName: string, errorType: string, index: number) {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const field = ingredients.controls[index].get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  addIngredient() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      amount: new FormControl(null, Validators.required),
      type: new FormControl('', Validators.required),
    })
    ingredients.push(ingredientGroup);
  }

  getIngControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  delete(i: number) {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    ingredients.removeAt(i);
  }

}
